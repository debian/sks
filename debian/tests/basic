#!/bin/bash

set -e

# avoid the network-based import, just use the debian keyring
for x in  /usr/share/keyrings/debian-*.gpg; do
    target="/var/lib/sks/dump/$(basename "$x" .gpg).pgp"
    runuser - debian-sks -c "cp $(printf %q "$x") $(printf %q "$target")"
done

runuser - debian-sks -c /usr/share/sks/sks-db-setup 2> "${AUTOPKGTEST_ARTIFACTS}/sks-db-setup.stderr"

# TODO: we should also test this on non-systemd systems
#systemctl enable sks 2>&1
#systemctl start sks
service sks start

export GNUPGHOME="$AUTOPKGTEST_TMP/gpg"
rm -rf "$GNUPGHOME"
mkdir -p -m 0700 "$GNUPGHOME"
echo keyserver hkp://127.0.0.1 > "${GNUPGHOME}/dirmngr.conf"
cat > "${GNUPGHOME}/gpg.conf" <<EOF
with-colons
batch
fixed-list-mode
quiet
EOF

# should not have the release key already:
if gpg --with-colons --list-keys ='Debian Stable Release Key (9/stretch) <debian-release@lists.debian.org>' 2>/dev/null; then
    printf 'We have the release key already!\n' >&2
    exit 1
fi

# test looking for keys on the keyserver
gpg --search 'Debian Stable Release Key (9/stretch) <debian-release@lists.debian.org>' >"${AUTOPKGTEST_ARTIFACTS}/stable-release-keyserver.output" 2>/dev/null

diff -u debian/tests/stable-release-keyserver.expected "${AUTOPKGTEST_ARTIFACTS}/stable-release-keyserver.output"

# test actually receiving keys from the keyserver

gpg --recv 067E3C456BAE240ACEE88F6FEF0F382A1A7B6500

gpg --with-colons --list-keys ='Debian Stable Release Key (9/stretch) <debian-release@lists.debian.org>' > "${AUTOPKGTEST_ARTIFACTS}/postimport-list.output"

diff -u debian/tests/postimport-list.expected <(grep -v ^tru < "${AUTOPKGTEST_ARTIFACTS}/postimport-list.output" | cut -d: -f1-18)

# TODO: test sending keys to the keyserver, and trying to recover them

# TODO: set up a second keyserver, configure recon between them

# TODO: test sending a key to one keyserver, ensuring that a new key is synchronized
