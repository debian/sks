Source: sks
Section: net
Priority: optional
Standards-Version: 4.1.3
Maintainer: Christoph Martin <martin@uni-mainz.de>
Build-Depends:
 camlp4,
 debhelper-compat (= 12),
 dh-ocaml (>= 0.9~),
 libcryptokit-ocaml-dev (>= 1.2-4),
 libdb-dev,
 libnum-ocaml-dev,
 ocaml (>= 3.08),
 ocaml-findlib,
 ocaml-nox (>= 1.3-4),
 perl,
 perl-doc,
 zlib1g-dev,
Vcs-Browser: https://salsa.debian.org/debian/sks
Vcs-Git: https://salsa.debian.org/debian/sks.git
Homepage: https://github.com/SKS-Keyserver/sks-keyserver
Rules-Requires-Root: no

Package: sks
Architecture: any
Depends:
 adduser,
 db-util,
 lsb-base (>=3.0-6),
 ${bdb:Depends},
 ${misc:Depends},
 ${ocaml:Depends},
 ${shlibs:Depends},
Provides:
 ${ocaml:Provides},
Suggests:
 logrotate,
 postfix | mail-transport-agent,
 procmail,
Description: Synchronizing OpenPGP Key Server
 SKS is an OpenPGP key server that correctly handles all OpenPGP features
 defined in RFC2440 and RFC4880, including photoID packages and multiple
 subkeys.
 .
 This key server implementation uses an efficient and reliable reconciliation
 algorithm to keep the database in sync with other SKS servers.  Additionally
 it can both send and receive PKS style sync emails.
