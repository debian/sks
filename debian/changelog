sks (1.1.6+git20210302.c3ba6d5a-4) unstable; urgency=medium

  * include tail-recursive-key-parsing.patch from
    https://github.com/SKS-Keyserver/sks-keyserver/pull/79
  * include Do-not-error-on-warning-6-labels-omitted.patch (closes: 1002657)

 -- Christoph Martin <martin@uni-mainz.de>  Mon, 24 Jan 2022 12:33:30 +0100

sks (1.1.6+git20210302.c3ba6d5a-3) unstable; urgency=medium

  * don't abort in cleandb call in sks_build.sh (closes: 975711)

 -- Christoph Martin <martin@uni-mainz.de>  Mon, 22 Mar 2021 10:50:41 +0100

sks (1.1.6+git20210302.c3ba6d5a-2) unstable; urgency=medium

  * reinclude 0011-non-interactive-use-of-sks_build.sh.patch (closes: #975711)

 -- Christoph Martin <martin@uni-mainz.de>  Sun, 21 Mar 2021 20:24:07 +0100

sks (1.1.6+git20210302.c3ba6d5a-1) unstable; urgency=medium

  * import upstream commits fixes dbt readonly error
  * add filter for poison keys

 -- Christoph Martin <martin@uni-mainz.de>  Tue, 02 Mar 2021 10:07:28 +0100

sks (1.1.6+git20200620.9e9d504-1) unstable; urgency=medium

  * import upstream commits
  * fixes build with safe-strings and ocaml > 4.08 (closes: #939962)
  * change upstream URL from bitbucket.org to github.com
  * fix some lintian issues

 -- Christoph Martin <martin@uni-mainz.de>  Wed, 16 Sep 2020 18:56:08 +0200

sks (1.1.6-15) unstable; urgency=medium

  [ Daniel Kahn Gillmor ]
  * removing myself from uploaders

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Add missing colon in closes line.

 -- Christoph Martin <martin@uni-mainz.de>  Tue, 15 Sep 2020 10:55:27 +0200

sks (1.1.6-14) unstable; urgency=medium

  * move to debhelper 11
  * Standards-Version: 4.1.3 (no changes needed)
  * move Vcs: * fields to salsa.debian.org
  * use DEP-14 repo branch naming scheme
  * d/control: add Rules-Requires-Root: no
  * debian/tests/basic: reverse the order of the diffs
  * d/changelog: clean trailing whitespace
  * autopkgtest:  no need to work around #870780 any more
  * trim noisy GnuPG fields
  * send systemctl enable stderr to stdout

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Fri, 23 Feb 2018 14:55:28 -0800

sks (1.1.6-13) unstable; urgency=medium

  * one more fix to autopkgtest

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Tue, 08 Aug 2017 13:31:41 -0400

sks (1.1.6-12) unstable; urgency=medium

  * /var/lib/sks/dump should be owned by debian-sks too
  * streamline automated setup
  * refresh git metadata in patches
  * disable by default on sysvinit-managed systems (same as systemd)
  * autopkgtest: work around #870780

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Mon, 07 Aug 2017 01:34:00 -0400

sks (1.1.6-11) unstable; urgency=medium

  * logrotate: use invoke-rc.d instead of explicit /etc/init.d
  * test: ensure that the dumps are owned by debian-sks
  * description: refer explicitly to RFC 4880
  * use systemd vendor presets to not enable sks daemons by default
  * sks-db-setup is now clearer about system integration suggestions
  * drop /etc/default/sks (see NEWS)
    - non-systemd systems should enable/disable sks via update-rc.d
  * clean up autopkgtest

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 02 Aug 2017 13:23:24 -0400

sks (1.1.6-10) unstable; urgency=medium

  * debian/tests/basic needs sks installed

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 02 Aug 2017 01:27:06 -0400

sks (1.1.6-9) unstable; urgency=medium

  * Pre-build Bdb library (fix parallel build) (Closes: #870398)
  * correct the autopkgtest

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Tue, 01 Aug 2017 22:07:10 -0400

sks (1.1.6-8) unstable; urgency=medium

  * Try a different mechanism for build parallelism and
    dependency-tracking

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Tue, 01 Aug 2017 18:14:58 -0400

sks (1.1.6-7) unstable; urgency=medium

  * allow building in parallel (Closes: #870398)

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Tue, 01 Aug 2017 16:16:47 -0400

sks (1.1.6-6) unstable; urgency=medium

  [ Stéphane Glondu ]
  * Fix FTBFS with OCaml 4.05.0 (Closes: #870150)

  [ Daniel Kahn Gillmor ]
  * bump Standards-Version to 4.0.0 (no changes needed)
  * wrap-and-sort -ast
  * implement basic autopkgtest

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Tue, 01 Aug 2017 10:20:24 -0400

sks (1.1.6-5) unstable; urgency=medium

  [ Daniel Kahn Gillmor]
  * move to debhelper 10

  [ Christoph Martin ]
  * compile with system zarith where cryptokit depends on (closes: #868631)

 -- Christoph Martin <martin@uni-mainz.de>  Wed, 26 Jul 2017 15:34:53 +0200

sks (1.1.6-4) unstable; urgency=medium

  * fix watch file

 -- Christoph Martin <christoph.martin@uni-mainz.de>  Wed, 09 Nov 2016 09:58:48 +0100

sks (1.1.6-3) unstable; urgency=medium

  * fix sks-build-db script. Thanks, Brian Minton (Closes: #835982)

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Mon, 29 Aug 2016 16:19:25 -0400

sks (1.1.6-2) unstable; urgency=medium

  * Make SKS pie (thanks, Stéphane Glondu!) (Closes: #834044)
  * minor improvements to /etc/init.d/sks (instigated by lintian)

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Fri, 12 Aug 2016 12:32:04 -0400

sks (1.1.6-1) unstable; urgency=medium

  * new upstream release

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Mon, 08 Aug 2016 02:36:22 -0400

sks (1.1.5-8) unstable; urgency=medium

  * avoid ftbfs on arches without a native ocamlopt

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Thu, 12 May 2016 02:25:22 -0400

sks (1.1.5-7) unstable; urgency=medium

  * re-include stock files in /var/lib/sks/www
  * fix typo in example config file.

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 11 May 2016 11:10:21 -0400

sks (1.1.5-6) unstable; urgency=medium

  * ship useful helper scripts: sks-db-setup and sks-db-upgrade
  * drop ancient tests in postinst, improve logic, and simplify by using
    sks-db-upgrade
  * avoid recursive chowns (Closes: #705725)
  * sks-db-upgrade will not fail when database or log files are absent
    (Closes: #750504)
  * remove the debian-sks user on purge (Closes: #778726)
  * add service dependencies and fix BindsTo typo (Closes: #823781)
  * ensure that /var/lib/sks exists (Closes: #823780)
  * update README.Debian to reflect these changes

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 11 May 2016 02:39:32 -0400

sks (1.1.5-5) unstable; urgency=medium

  * canonicalize header files
  * bump to Standards-Version 3.9.8 (no changes needed)
  * move logrotate from Depends: to Suggests:
  * pull upstream fixes, and changes that support handling Curve 25519 keys
  * make sysvinit script depend on $network (Closes: #822248)
  * Depend: on db-util so that cronjob can clean up (Closes: #795080)
  * added systemd service files (Closes: #715360)

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Fri, 06 May 2016 20:10:29 -0400

sks (1.1.5-4) unstable; urgency=low

  * make sks depend on the exact dbx.x-util version (closes: #774368)

 -- Christoph Martin <christoph.martin@uni-mainz.de>  Thu, 22 Jan 2015 22:22:44 +0100

sks (1.1.5-3) unstable; urgency=medium

  * fix postrm if old /var/backup does not exist (closes: #718530)

 -- Christoph Martin <christoph.martin@uni-mainz.de>  Tue, 04 Nov 2014 12:12:10 +0100

sks (1.1.5-2) unstable; urgency=low

  * use start-stop-daemon with --background and --no-close instead of with
    & (closes: #765172)
  * change /var/backup/sks to /var/backups/sks for upgrade backups
    (closes: #718530)

 -- Christoph Martin <christoph.martin@uni-mainz.de>  Wed, 15 Oct 2014 17:10:51 +0200

sks (1.1.5-1) unstable; urgency=low

  [ Christoph Martin ]
  * new upstream
    - fixes CVE-2014-3207: non-persistent XSS (closes: 746626)
    - correctly handle option max_matches (closes: 742916)
    - correct documentation of dump command (closes: 600194)
  * add pgp signature option to watch file
  * remove /var/lib/sks and /var/backup/sks on purge (closes: 716838)
  * note active Berkely DB on new install (closes: 741912)

 -- Christoph Martin <christoph.martin@uni-mainz.de>  Fri, 16 May 2014 15:54:30 +0200

sks (1.1.4-2.1) unstable; urgency=high

  * Non-maintainer upload.
  * Fix compilation with OCaml 4.01.0 (Closes: #731584)

 -- Stéphane Glondu <glondu@debian.org>  Tue, 10 Dec 2013 07:18:02 +0100

sks (1.1.4-2) unstable; urgency=low

  * move to unstable
  * 1.1.4 stops creating gmon.out (Closes: #699847)

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Thu, 01 Aug 2013 23:04:12 -0400

sks (1.1.4-1) experimental; urgency=low

  * New Upstream Release (Closes: #690135)
  * added myself to Uploaders.
  * convert to dh 9
  * Standards-Version: bump to 3.9.4 (no changes needed)
  * debian/rules: clean up
  * refresh and clean up debian/patches
  * switch packaging vcs to git
  * avoid trying to upgrade DB_CONFIG (Closes: #709322)

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Thu, 27 Jun 2013 16:39:02 -0400

sks (1.1.3-3) UNRELEASED; urgency=low

  [ Christoph Martin ]
  * merge Ubuntu changes
  * Look for ^.i not ^ii in libdbX.Y-dev packages for BDB_VERSION. And
    assert that we have a value (LP: #1021650)
  * Create /var/lib/sks/berkeley_db.active even when we aren't upgrading BDB
    versions.

  [ Daniel Kahn Gillmor ]
  * avoid trying to upgrade DB_CONFIG (Closes: #709322)
  * switch to git
  * adding myself to uploaders
  * use debhelper 9

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Fri, 28 Jun 2013 18:10:07 -0400

sks (1.1.3-2) unstable; urgency=high

  * add Vcs tags to control file
  * fix watch file because of upstream move
  * add Homepage tag to control file
  * add db parameter to sksconf to fix db build deadlocks (closes: #699848)

 -- Christoph Martin <christoph.martin@uni-mainz.de>  Mon, 11 Mar 2013 16:48:16 +0100

sks (1.1.3-1) unstable; urgency=low

  * New upstream release (closes: #663757)
  * change patches to work with new release
  * include sample config and web
  * include default index.html and robots.txt (closes: #600008)

 -- Christoph Martin <christoph.martin@uni-mainz.de>  Wed, 20 Jun 2012 11:58:47 +0200

sks (1.1.1+dpkgv3-8) unstable; urgency=low

  * fix debian/watch
  * drop recommends on db4.7-utils (closes: 664768) Hopyfully you have the
    db-utils from you old db version installed. But it should also work
    without that.
  * change default mailsync server (closes: 599919)
  * change init script to not background sks too early to see error
    messages (closes: 651843)
  * bump Standards-Version to 3.9.1 (no changes)

 -- Christoph Martin <christoph.martin@uni-mainz.de>  Fri, 11 May 2012 17:15:08 +0200

sks (1.1.1+dpkgv3-7.1) unstable; urgency=low

  * Non-Maintainer Upload.
  * emit proper HTTP 1.0 POSTs from recon process (Closes: #667695)

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Fri, 06 Apr 2012 10:36:48 -0400

sks (1.1.1+dpkgv3-7) unstable; urgency=low

  * remove default mailsync entry to prevent sks from sending email syncs
    per default (Thanks to Graham Dunn <grdunn@rim.com> for the hint)
  * fix sks.daily to not depend on db4.7 (closes: #627767)
  * fix sks.postinst to only upgrade the db if the version changes

 -- Christoph Martin <christoph.martin@uni-mainz.de>  Tue, 31 May 2011 17:05:32 +0200

sks (1.1.1+dpkgv3-6.2) unstable; urgency=low

  * Non-maintainer upload.
  * Handle gracefully missing db directory, which can happen if:
    + sks was not enabled and didn't create PTree directory
    + keys haven't been imported to the package
  * Update instructions for retrieving keydump (Closes: #605454)
  * Install README from source directory (Closes: #599868)
  * Use dh_ocaml >= 0.9 (Closes: #599579)
  * Add hints to run commands as debian-sks user as suggested in the BTS

 -- Ondřej Surý <ondrej@debian.org>  Thu, 19 May 2011 11:56:33 +0200

sks (1.1.1+dpkgv3-6.1) unstable; urgency=low

  * Non-maintainer upload.
  * Remove XA environment usage (patch from upstream) and use generic -ldb
    library to link with (Closes: #621384)
  * Add automagick upgrade of Berkeley DB databases (Closes: #606183)
  * Change db4.7-util to Recommends: db4.7-util and Depends: db-util

 -- Ondřej Surý <ondrej@debian.org>  Fri, 13 May 2011 12:33:13 +0200

sks (1.1.1+dpkgv3-6) unstable; urgency=high

  * fix to not really working fix for strip of bytecode (closes: 599029)

 -- Christoph Martin <christoph.martin@uni-mainz.de>  Mon, 25 Oct 2010 18:11:37 +0200

sks (1.1.1+dpkgv3-5) unstable; urgency=high

  * add nostrip to DEB_BUILD_OPTIONS on architectures with ocaml
    bytecode. (closes: 599029)
  * don't depend on mta etc., as mailsync is now mostly obsolete (closes:
    599280)

 -- Christoph Martin <christoph.martin@uni-mainz.de>  Thu, 07 Oct 2010 20:49:13 +0200

sks (1.1.1+dpkgv3-4) unstable; urgency=low

  * remove pramberger.at from README.Debian, because it is no longer
    availlable (closes: #597818)

 -- Christoph Martin <christoph.martin@uni-mainz.de>  Mon, 27 Sep 2010 12:09:03 +0200

sks (1.1.1+dpkgv3-3) unstable; urgency=low

  * fix typo in control to include correct ocaml-base-nox dependency
    (closes: #596563)

 -- Christoph Martin <christoph.martin@uni-mainz.de>  Sun, 12 Sep 2010 21:14:26 +0200

sks (1.1.1+dpkgv3-2) unstable; urgency=low

  * fix build and install to work on plattforms which don't have the
    native ocaml compile (closes: #594557)

 -- Christoph Martin <christoph.martin@uni-mainz.de>  Fri, 27 Aug 2010 12:09:33 +0200

sks (1.1.1+dpkgv3-1) unstable; urgency=high

  * include NEWS file to inform about db update (closes: #594103)
  * fix README.Debian to include a working download URL for a fresh
    keydump (closes: 593852)
  * 511_gcc4.4_prototypes: Don't redefine bzero(3), fixes FTBFS (Thanks to
    Daniel T Chen <crimsun@ubuntu.com>)
  * convert to source format 3 with quilt patches (closes: #576057)

 -- Christoph Martin <christoph.martin@uni-mainz.de>  Wed, 25 Aug 2010 18:11:26 +0200

sks (1.1.1-2) unstable; urgency=low

  * don't chown PIDDIR in postinst, since we do it in init (Closes: #566922)

 -- Christoph Martin <christoph.martin@uni-mainz.de>  Mon, 08 Feb 2010 15:00:34 +0100

sks (1.1.1-1) unstable; urgency=low

  * New upstream
  * fix watchfile
  * fix some lintian errors/warnings

 -- Christoph Martin <christoph.martin@uni-mainz.de>  Sat, 28 Nov 2009 15:38:39 +0100

sks (1.1.0-9) unstable; urgency=low

  * Explicitly require libdb4.7, 4.8 is not supported (Closes: #549798).

 -- Christoph Martin <christoph.martin@uni-mainz.de>  Wed, 04 Nov 2009 21:40:36 +0100

sks (1.1.0-8) unstable; urgency=low

  * Teach dbserver about content-type for .html.
  * Allow - in filenames for webserver.
  * closes: #505014
  * thanks to Peter Palfrader <weasel@debian.org>

 -- Christoph Martin <christoph.martin@uni-mainz.de>  Thu, 16 Jul 2009 23:33:21 +0200

sks (1.1.0-7) unstable; urgency=low

  * fix patch for PIDDIR
  * link against libdb4.7
  * really include watch file

 -- Christoph Martin <christoph.martin@uni-mainz.de>  Thu, 16 Jul 2009 00:22:06 +0200

sks (1.1.0-6) unstable; urgency=low

  * remove Peter Palfrader from Maintainer and Uploaders
  * add watch file
  * Remove reference to native-arch in old OCaml standard library path
    (Thanks to Stephane Glondu <steph@glondu.net>) (Closes: #535267)

 -- Christoph Martin <christoph.martin@uni-mainz.de>  Fri, 10 Jul 2009 23:34:36 +0200

sks (1.1.0-5) unstable; urgency=low

  * Check spool directories for existance (Thanks to Filippo Giunchedi
    <filippo@debian.org>) (Closes: #493335)
  * make PIDDIR on startup to workaround /var/run on a tempfs

 -- Christoph Martin <christoph.martin@uni-mainz.de>  Thu, 18 Jun 2009 16:39:09 +0200

sks (1.1.0-4) unstable; urgency=low

  * fix cron.daily to call correct db*_archive and print unwanted messages
    (closes: #491281)
  * fix comment in README.Debian about correcting permissions after
    initial database build

 -- Christoph Martin <christoph.martin@uni-mainz.de>  Tue, 22 Jul 2008 17:27:58 +0200

sks (1.1.0-3) unstable; urgency=low

  * make separate patches for manpage install for upstream
  * patch to correctly build bdb on non native ocaml archs

 -- Christoph Martin <christoph.martin@uni-mainz.de>  Tue, 24 Jun 2008 17:16:49 +0200

sks (1.1.0-2) unstable; urgency=low

  * Make sks build on architectures which are non native ocaml archs
    (closes: #486847)
  * update to standards version 3.8.0
  * some changes to README.Debian

 -- Christoph Martin <christoph.martin@uni-mainz.de>  Tue, 24 Jun 2008 11:01:10 +0200

sks (1.1.0-1) unstable; urgency=low

  * First official Debian release (closes: #484785)
  * New upstream release
  * Update to newest standards version and sid environment
  * Change to using db4.6
  * Drop usage of numerix
  * Include latest upstream patches
  * Install manpage

 -- Christoph Martin <christoph.martin@uni-mainz.de>  Wed, 11 Jun 2008 16:55:15 +0200

sks (1.0.10-0.1) UNRELEASED; urgency=low

  * New upstream release.
  * Several changes courtesy of Marco Nenciarini <mnencia@debian.org>.
  * Add some comments to etc/sks/{membership,mailsyn,sksconf}.
  * New upstream patch incorporates the following patches:
    - 212_do_not_export_zerolen_packets
  * Update 500_debian_fhs for new log filenames.
  * Remove 510_sane_logfile_names.
  * Rename logfiles in postinst.
  * Forward port rest of 500_debian_fhs.
  * Forward port 501_makefile_cflags.
  * Remove 511_sys_random, it looks obsolete.
  * Add debian/contrib/make_debian_orig script by Marco to build
    debian upstream tarballs without the unneeded crap put into the
    upstreamtarball.

 -- Peter Palfrader <weasel@debian.org>  Wed, 24 Aug 2005 18:55:04 +0200

sks (1.0.9-0.6) UNRELEASED; urgency=low

  * Add 212_do_not_export_zerolen_packets.

 -- Peter Palfrader <weasel@debian.org>  Thu, 18 Aug 2005 03:02:48 +0200

sks (1.0.9-0.5) UNRELEASED; urgency=low

  * Fix 500_debian_fhs again.

 -- Peter Palfrader <weasel@debian.org>  Thu, 14 Apr 2005 09:04:42 +0200

sks (1.0.9-0.4) UNRELEASED; urgency=low

  * Fix 500_debian_fhs, so that we do not create a thousand
    diff-xxx files because client ports in recon are changing
    all the time.  Upstream got that right, but the patch
    reverted that to a (previous? and) broken behaviour.

 -- Peter Palfrader <weasel@debian.org>  Thu, 20 Jan 2005 17:18:58 +0100

sks (1.0.9-0.3) UNRELEASED; urgency=low

  * Changed maintainer to myself.
    the debian-sks@mirror address was bouncing.

 -- Peter Palfrader <weasel@debian.org>  Wed,  5 Jan 2005 19:19:28 +0100

sks (1.0.9-0.2) UNRELEASED; urgency=low

  * Make /var/log/sks readable by group adm.
  * Fix 500_debian_fhs to also change the other location in the code
    where diff-* files are used.

 -- Peter Palfrader <weasel@debian.org>  Fri, 12 Nov 2004 17:27:41 +0100

sks (1.0.9-0.1) UNRELEASED; urgency=low

  * New upstream release.
  * New upstream patch incorporates the following patches:
    - 208_show_revoked_in_ms
    - 209_handle_attribute_uids_in_mr
    - 210_only_latest_changetime_in_mr
    - 211_content_type_utf8
  * Upstream has a changelog again.
  * Upstream shipped manpage as pod, build-depend on perl and perl-doc
    so the Makefile can build it.
  * The build script reappeared in the upstream tarball, resurrect
    508_build_fastbuild.

 -- Peter Palfrader <weasel@debian.org>  Tue, 26 Oct 2004 01:42:34 +0200

sks (1.0.8-0.1) UNRELEASED; urgency=low

  * New upstream release.
  * New upstream patch incorporates the following patches:
     - 201_speling
     - 202_correct_x_keyserver
     - 203_extra_space_in_fpr
     - 204_relative_links
     - 205_show_peer_when_config_rejected
     - 206_also_write_empty_diffs
     - 207_log_recovered_hashes_source
  * The build script disappeared in the upstream tarball, remove 508_build_fastbuild
  * Updated with new pathnames/offsets:
     - 209_handle_attribute_uids_in_mr
     - 210_only_latest_changetime_in_mr
     - 402_separate_keys_with_hr
     - 500_debian_fhs
     - 501_makefile_cflags
     - 509_Slong_Dlong
     - 510_sane_logfile_names
  * Use UTF8 as charset in html content-types (211_content_type_utf8)
  * Correct name of external variable so it works properly with ocaml 3.08
    and nox.
  * Update build dependencies to build against ocaml 3.08.  Includes changing
    from libnums to ocaml-nox.
  * Upstream now has a manpage, use that instead of our sks.8.
  * Upstream has no changelog right now!!

 -- Peter Palfrader <weasel@debian.org>  Mon, 18 Oct 2004 23:41:42 +0200

sks (1.0.7-0.4) UNRELEASED; urgency=low

  * add 211_content_type_utf8 to have utf8 encoding with tex/html.

 -- Peter Palfrader <weasel@debian.org>  Mon, 18 Oct 2004 22:17:30 +0200

sks (1.0.7-0.3) UNRELEASED; urgency=low

  * add 208_show_revoked_in_ms: show key revoked information in machine
    readable output.
  * add 209_handle_attribute_uids_in_mr: do print "uat" instead of "uid" and
    do not dump binary garbage to the client.
  * add 210_only_latest_changetime_in_mr: in machine readable output, do
    not show list of create and expiration times, only one.

 -- Peter Palfrader <weasel@debian.org>  Mon,  2 Aug 2004 11:03:43 +0200

sks (1.0.7-0.2) UNRELEASED; urgency=low

  * add 205_show_peer_when_config_rejected: print peername in reject errors.
  * add 206_also_write_empty_diffs: also write empty diff-<ip>.txt files.
  * add 207_log_recovered_hashes_source: show source of keys in recon.

 -- Peter Palfrader <weasel@debian.org>  Sat, 19 Jun 2004 01:21:14 +0200

sks (1.0.7-0.1) UNRELEASED; urgency=low

  * packaging sks.

 -- Peter Palfrader <weasel@debian.org>  Sun, 14 Mar 2004 01:46:23 +0100
